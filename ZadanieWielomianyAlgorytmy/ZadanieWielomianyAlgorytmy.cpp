﻿#include "includes.h"

using std::cout;
using std::cin;
using std::milli;
using std::chrono::duration;
typedef std::chrono::high_resolution_clock Clock;

double ManualMethod(double* tab, unsigned int n, double arg)
{
    double result = 0;
    auto counter = n;
    for (size_t i = 0; i < n; i++) {
        result += tab[i] * pow(arg, counter - 1);
        counter--;
    }
    return result;
}

double HornerMethod(double* tab, unsigned int n, double arg)
{
    double result = tab[0];
    for (size_t i = 1; i < n; i++)
        result = result * arg + tab[i];

    return result;
}

double GetRandNum() {
    std::mt19937 mTw; // Mersenne Twister
    auto seedTime = Clock::now().time_since_epoch().count();
    mTw.seed(seedTime);
    std::uniform_real_distribution<double> uniform(0, 1);
    return uniform(mTw);
}

unsigned int polynomialDegree;

int main()
{
    cout.precision(30);
    // specs
    unsigned short tablesCount = 3;
    const size_t numOfElements[4] = { 100, 10000, 100000, 2000000 }; // , 2000000
    const short numOfCycles = 10;
    bool useBigTable = false;

    cout << "Podaj stopien wielomianu: \n";
    cin >> polynomialDegree;
    cout << "Czy uzywac tablicy 2 mln elementow? (1 - tak, 0 - nie) \n";
    cin >> useBigTable;
    if (useBigTable && tablesCount < 4)
        tablesCount++;
    auto numOfElementsLen = *(&numOfElements + 1) - numOfElements;
    if (polynomialDegree > 0 && cin.good())
    {
        // tables memory initialization
        double** pTable = new double* [numOfElementsLen];
        for (size_t tableIterator = 0; tableIterator < numOfElementsLen; tableIterator++)
            pTable[tableIterator] = new double[numOfElements[tableIterator]]();
        // fill tables with random data
        if (useBigTable)
            cout << "Trwa zapelnianie tablic, moze to chwile potrwac... \n";
        for (size_t tableIterator = 0; tableIterator < tablesCount; tableIterator++) {
            for (auto tableIndex = 0; tableIndex < numOfElements[tableIterator]; tableIndex++)
                pTable[tableIterator][tableIndex] = GetRandNum();
        }
        for (size_t tableIteration = 0; tableIteration < tablesCount; tableIteration++) {
            for (size_t cycleIteration = 0; cycleIteration < numOfCycles; cycleIteration++) {
                cout << "Cykl nr." << cycleIteration + 1 + tableIteration * 10 << "\n";
                // variables
                double  hornerResult, 
                        manualResult;
                double durationHorner = 0,
                        durationManual = 0;
                auto argument = GetRandNum();
                // Horner method
                auto t1 = Clock::now();
                hornerResult = HornerMethod(pTable[tableIteration], polynomialDegree, argument);
                auto t2 = Clock::now();        
                durationHorner = duration<double, milli>(t2 - t1).count();
                // Manual method
                t1 = Clock::now();
                manualResult = ManualMethod(pTable[tableIteration], polynomialDegree, argument);
                t2 = Clock::now();
                durationManual = duration<double, milli>(t2 - t1).count();             
                // output results
                cout << "Wynik rownania hornera: " << hornerResult << "ms \n";
                cout << "Wynik rownania manualnie: " << manualResult << "ms \n";
                cout << "Czas wykonania hornera: " << durationHorner << " ms \n";
                cout << "Czas wykonania manualnie: " << durationManual << "ms \n";
            }
        }
        // dispose memory
        if (pTable != NULL) {
            for (int i = 0; i < tablesCount; ++i) {
                delete[] pTable[i];
            }
            delete[] pTable;
        }
        cin.get();
        cin.get();
        return 0;
        
    }
    else{
        cin.clear();
        cout << "Podaj prawidlowy stopien wielomianu! \n";
        if(numOfElements != NULL)
            delete[] numOfElements;
        return main();
    }
    cin.get();
    cin.get();
    return 0;
}
