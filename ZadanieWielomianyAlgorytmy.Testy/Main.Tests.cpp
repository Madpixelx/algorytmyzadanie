#include "pch.h"
#include "CppUnitTest.h"
#include "../ZadanieWielomianyAlgorytmy/ZadanieWielomianyAlgorytmy.cpp"
#include <list>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ZadanieWielomianyAlgorytmyTesty
{
	TEST_CLASS(ZadanieWielomianyAlgorytmyTesty)
	{
	public:
		
		TEST_METHOD(Horner_ResultEquals_Manual){
			auto numOfElements = 150;
			double** pTable = new double* [1];
			pTable[0] = new double[numOfElements]();
			// fill table
			for (auto tableIndex = 0; tableIndex < numOfElements; tableIndex++)
					pTable[0][tableIndex] = GetRandNum();

			std::list<bool> results = std::list<bool>();

			auto arg = GetRandNum();
			auto polynomial = 10;

			auto manualResult = ManualMethod(pTable[0], polynomial, arg);
			auto hornerResult = HornerMethod(pTable[0], polynomial, arg);
			if (!fabs(manualResult - hornerResult) < 0.01)
				Assert::Fail();
		}
		TEST_METHOD(HornerMethod_equals6){
			double Table[] = {3,3,2};
			auto hornerResult = HornerMethod(Table, 3, 1);
			Assert::IsTrue(hornerResult == 8);
		}
		TEST_METHOD(HornerMethod_equals5){
			double Table[] = { 2, -6, 2, -1 };
			auto hornerResult = HornerMethod(Table, 4, 3);
			Assert::IsTrue(hornerResult == 5);
		}
		TEST_METHOD(ManualMethod_equals5){
			double Table[] = { 2, -6, 2, -1 };
			auto manualResult = ManualMethod(Table, 4, 3);
			Assert::IsTrue(manualResult == 5);
		}
		TEST_METHOD(GetRandNum_isUnique) {
			for (size_t i = 0; i < 20; i++)
			{
				if (GetRandNum() == GetRandNum())
					Assert::Fail();
			}
		}
	};
}
